module.exports = function (grunt) {
    require('jit-grunt')(grunt);

    var config = {
        pkg: grunt.file.readJSON('package.json'),

        // typescript: {
        //     expressServer: {
        //         src: ['backend/src/*.ts'],
        //         dest: 'backend/js',
        //         options: {
        //             module: 'amd',
        //             target: 'es5'//,
        //             //sourceMap: true,
        //             //declarations: true
        //         }
        //     }
        // },
        // browserify: {
        //     options: {
        //         transform: [require('brfs')],
        //         browserifyOptions: {
        //             basedir: "Frontend/src/js/"
        //         }
        //     },
        //     rangePage: {
        //         src: 'Frontend/src/rangePage.js',
        //         dest: 'Frontend/www/assets/js/rangePage.js'
        //     },
        //     orderPage: {
        //         src: 'Frontend/src/orderPage.js',
        //         dest: 'Frontend/www/assets/js/orderPage.js'
        //     }
        // },
        less: {
            development: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    "frontend/src/css/mainPage.css": "frontend/src/less/mainPage.less"
                }
            }
        }
    };

    // //Налаштування відстежування змін в проекті
    // var watchDebug = {
    //     options: {
    //         'no-beep': true
    //     },
    //     //Назва завдання будь-яка
    //     scripts: {
    //         //На зміни в яких файлах реагувати
    //         files: ['Frontend/src/**/*.js', 'Frontend/**/*.ejs'],
    //         //Які завдання виконувати під час зміни в файлах
    //         tasks: ['browserify:pizza']
    //     }
    // };


    //Ініціалузвати Grunt
    //config.watch = watchDebug;
    grunt.initConfig(config);

    grunt.loadNpmTasks('grunt-browserify');
    //grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.loadNpmTasks('grunt-typescript');

    grunt.registerTask('default',
        [
            //'typescript:expressServer',
            'less'
        ])
    // grunt.registerTask('default',
    //     [
    //         'browserify:rangePage',
    //         'browserify:orderPage',
    //         'less'
    //     ]
    // );

};