//
//  Created by Oleh Kurachenko
//         aka okurache
//  on 2018-03-24T12:29:06Z as a part of intra_mockup
//  
//  ask me      oleh.kurachenko@gmail.com , I'm ready to help
//  GitHub      https://github.com/OlehKurachenko
//  rate & CV   http://www.linkedin.com/in/oleh-kurachenko-6b025b111
// 

import app from './app'

const port = 8090;

app.listen(port, (err) => {
    if (err) {
        return console.log(err);
    }
    return console.log(`server is listening on ${port}`)
});