//
//  Created by Oleh Kurachenko
//         aka okurache
//  on 2018-03-24T12:28:18Z as a part of intra_mockup
//  
//  ask me      oleh.kurachenko@gmail.com , I'm ready to help
//  GitHub      https://github.com/OlehKurachenko
//  rate & CV   http://www.linkedin.com/in/oleh-kurachenko-6b025b111
// 

import * as express from 'express'
import * as morgan from 'morgan'

class App {
    public express;

    constructor() {
        this.express = express();

        this.express.set('views', '../../frontend/src/ejs');
        this.express.set('view engine', 'ejs');

        this.express.use(morgan('dev'));

        this.initRoutes();
    }

    private initRoutes(): void {
        this.express.get('/', (req, res) => {
            res.render('mainPage');
        });

        this.express.use(express.static('../../frontend/src'));
    }
}

export default new App().express